#include <cstdlib>
#include <cassert>
#include <cstring>

#define STATE_SZ 5
#define NODES_N (1 << 18)

typedef struct Node_s {
  char state[STATE_SZ];
  size_t cost;
  struct Node_s *parent;
} Node;

Node *uniform_cost_search(char start_state[STATE_SZ], char goals[][STATE_SZ], size_t n_goals,
			  size_t (*p_successors)(Node *nodes, Node *from_node, size_t *successor_count, size_t *i_nodes));

typedef enum {
  A = 0,
  B,
  C,
  STORE,
} Pos;

size_t successors(Node *nodes, Node *from_node, size_t *successor_count,  size_t *i_nodes) {
  size_t start_index = *i_nodes;
  size_t count = 0;
  size_t pos = from_node->state[4];
  // load
  for(size_t load = 1; load <= 3; ++load) {
    memcpy(&nodes[start_index + count], from_node, sizeof(Node));
    nodes[start_index + count].parent = from_node;

    nodes[start_index + count].cost += load;
    if(pos != STORE) {
      nodes[start_index + count].state[pos] -= load;
    }
    nodes[start_index + count].state[3] += load;
    if ((nodes[start_index + count].state[3] > 3) || (nodes[start_index + count].state[pos] < 0)) break;
    else count++;
  }

  // unload
  for(size_t load = 1; load <= 3; ++load) {
    memcpy(&nodes[start_index + count], from_node, sizeof(Node));
    nodes[start_index + count].parent = from_node;

    nodes[start_index + count].cost += load;
    if(pos != STORE) {
      nodes[start_index + count].state[pos] += load;
    }
    nodes[start_index + count].state[3] -= load;
    if (nodes[start_index + count].state[3] < 0) break;
    else count++;
  }

  // move
  // init
  static char in_mat[4][4] = {0};
  // init
  if (in_mat[STORE][A] != 6) {
    in_mat[STORE][A] = 6;
    in_mat[A][STORE] = 6;
    in_mat[STORE][B] = 4;
    in_mat[B][STORE] = 4;
    in_mat[A][B] = 7;
    in_mat[B][A] = 7;
    in_mat[A][C] = 5;
    in_mat[C][A] = 5;
    in_mat[B][C] = 4;
    in_mat[C][B] = 4;
  }
  for(size_t i = 0; i < 4; ++i) {
    size_t cost = in_mat[pos][i];
    if(cost != 0) {
      memcpy(&nodes[start_index + count], from_node, sizeof(Node));
      nodes[start_index + count].parent = from_node;
      nodes[start_index + count].state[4] = i;
      nodes[start_index + count].cost += cost;
      count++;
    }
  }
  *i_nodes += count;
  *successor_count = count;
  return start_index;
}

#include <stdio.h>

int main(int argc, char *argv[]) {
  char start_state[5] = {0, 0, 0, 0, STORE};
  char goal_states[3][5] = {
    {3, 4, 4, 0, A},
    {3, 4, 4, 0, B},
    {3, 4, 4, 0, C},
  };

  Node *solution = uniform_cost_search(start_state, goal_states, 3, successors);
  printf("A, B, C, TRUCK, POS\n");
  while(solution) {
    for(size_t i = 0; i < 5; ++i) {
      printf("%d, ", solution->state[i]);
    }
    printf("cost: %ld", solution->cost);
    printf("\n");
    solution = solution->parent;
  }
  return 0;
}

/********************************
 *          HASHMAP
 ********************************/
#define HASH_TABLE_SZ (NODES_N)
#define HASH_LOAD_FACTOR (0.75)

// cost -1 (underflowed) equals to bucket is empty
typedef struct {
  char state[STATE_SZ];
  size_t cost;
} HashBucket;

typedef struct {
  HashBucket *table;
  size_t count;
} HashMap;

HashMap hashmap_create(void) {
  HashMap hashmap;
  hashmap.count = 0;
  hashmap.table = (HashBucket *)calloc(sizeof(HashBucket), HASH_TABLE_SZ);
  for(size_t i = 0; i < HASH_TABLE_SZ; ++i) {
    hashmap.table[i].cost = -1;
  }
  return hashmap;
}

size_t hash(char state[STATE_SZ]) {
  size_t hash = 2166136261;
  for(char i = 0; i < STATE_SZ; ++i) {
    hash = (hash * 16777619) % HASH_TABLE_SZ;
    hash = hash ^ state[i];
  }
  return hash;
}

inline size_t probe(size_t hash, size_t i) {
  return (hash + (i >> 1) + ((i * i) >> 1)) % HASH_TABLE_SZ;
}

size_t hashmap_get(HashMap *hashmap, char state[STATE_SZ], size_t *ret_i) {
  HashBucket *table = hashmap->table;
  size_t hash_val = hash(state);
  size_t i = hash_val;
  for(size_t count = 1;
      memcmp(table[i].state, state, sizeof(char) * STATE_SZ) != 0
	&& table[i].cost != (size_t) -1;
      ++count) {
    i = probe(hash_val, count);
  }
  *ret_i = i;
  return table[i].cost;
}

void hashmap_set(HashMap *hashmap, char state[STATE_SZ], size_t cost) {
  HashBucket *table = hashmap->table;
  size_t i = 0;
  size_t bucket_cost = hashmap_get(hashmap, state, &i);
  if (bucket_cost == (size_t) -1) {
    assert((double)(hashmap->count) < (double)HASH_TABLE_SZ / HASH_LOAD_FACTOR);
    hashmap->count++;
  } 
  memcpy(table[i].state, state, sizeof(char) * STATE_SZ);
  table[i].cost = cost;
}

/********************************
 *           QUEUE
 ********************************/
#define QUEUE_SZ (NODES_N / (1 << 6))

typedef struct {
  Node *heap[QUEUE_SZ];
  size_t sz;
} Queue;

inline void swap(Node **heap, size_t i, size_t k) {
  Node *tmp = heap[i];
  heap[i] = heap[k];
  heap[k] = tmp;
}

Queue queue_create(void) {
  return {.sz = 0};
}

void heapify_up(Queue *queue, size_t i) {
  if(i == 0) return;
  Node **heap = queue->heap;
  size_t parent_i = (i-1) / 2;
  if(heap[parent_i]->cost > heap[i]->cost) {
    swap(heap, parent_i, i);
    heapify_up(queue, parent_i);
  }
}

void queue_push(Queue *queue, Node *node) {
  assert(queue->sz < QUEUE_SZ);
  queue->heap[queue->sz] = node;
  heapify_up(queue, queue->sz);
  queue->sz++;
}

void heapify_down(Queue *queue, size_t i) {
  Node **heap = queue->heap;
  size_t left_i = 2 * i + 1;
  size_t right_i = 2 * i + 2;
  size_t left_cost = left_i < queue->sz ? queue->heap[left_i]->cost : (size_t) -1;
  size_t right_cost = right_i < queue->sz ? queue->heap[right_i]->cost : (size_t) -1;
  size_t smallest_i = (left_cost <= right_cost) ? left_i : right_i;
  if(smallest_i >= queue->sz) return;
  if(heap[smallest_i]->cost < heap[i]->cost) {
    swap(heap, smallest_i, i);
    heapify_down(queue, smallest_i);
  }
}

Node *queue_pop(Queue *queue) {
  if(queue->sz == 0) return NULL;
  queue->sz--;
  Node *ret_node = queue->heap[0];
  swap(queue->heap, 0, queue->sz);
  heapify_down(queue, 0);
  return ret_node;
}

/********************************
 *         HASHQUEUE
 ********************************/
typedef struct {
  HashMap hashmap;
  Queue queue;
} HashQueue;

HashQueue hashqueue_create(void) {
  HashQueue hashqueue;
  hashqueue.hashmap = hashmap_create();
  hashqueue.queue = queue_create();
  return hashqueue;
}

void hashqueue_push_valid(HashQueue *hashqueue, Node *node) {
  HashMap *hashmap = &hashqueue->hashmap;
  size_t hash_i = 0;
  size_t cost = hashmap_get(hashmap, node->state, &hash_i);
  if(node->cost < cost) {
    queue_push(&hashqueue->queue, node);
    HashBucket *bucket = &(hashmap->table[hash_i]);
    memcpy(bucket->state, node->state, sizeof(char) * STATE_SZ);
    bucket->cost = node->cost;
    if(cost == (size_t) -1) hashmap->count++;
  }
}

Node *hashqueue_pop_valid(HashQueue *hashqueue) {
  HashMap *hashmap  = &hashqueue->hashmap;
  Node *node;
  size_t cost;
  size_t dummy;
  do {
    node = queue_pop(&hashqueue->queue);
    if(!node) return NULL;
    cost = hashmap_get(hashmap, node->state, &dummy);
    assert(cost != (size_t) -1);
  } while (node->cost != cost);
  return node;
}

/********************************
 *          HELPERS
 ********************************/
void copy_state(char *to_state, char *from_state) {
  for(size_t i = 0; i < STATE_SZ; ++i) {
    to_state[i] = from_state[i];
  }
}

bool equal_any(char state[STATE_SZ], char goals[][STATE_SZ], size_t n_goals) {
  for(size_t i = 0; i < n_goals; ++i) {
    if(memcmp(state, goals[i], sizeof(char) * STATE_SZ) == 0) return true;
  }
  return false;
}

/********************************
 *    UNIFORM COST SEARCH
 ********************************/
Node *uniform_cost_search(char start_state[STATE_SZ], char goals[][STATE_SZ], size_t n_goals,
			  size_t (*p_successors)(Node *nodes, Node *from_node, size_t *successor_count, size_t *i_nodes)) {
  Node *nodes = (Node *) malloc(sizeof(Node) * NODES_N);
  HashMap expanded = hashmap_create();
  HashQueue frontier = hashqueue_create();

  size_t dummy;
  size_t successor_count;
  size_t i_nodes = 0;
  copy_state(nodes[i_nodes].state, start_state);
  nodes[i_nodes].cost = 0;
  nodes[i_nodes].parent = NULL;

  hashqueue_push_valid(&frontier, nodes + i_nodes);

  i_nodes++;
  while (1) {
    Node *node = hashqueue_pop_valid(&frontier);
    if (!node) return NULL;
    if (equal_any(node->state, goals, n_goals)) return node;

    hashmap_set(&expanded, node->state, 1);
    size_t i_successors = p_successors(nodes, node, &successor_count, &i_nodes);
    assert(i_nodes < NODES_N);
    for(size_t i = 0; i < successor_count; ++i) {
      if (hashmap_get(&expanded, nodes[i_successors + i].state, &dummy) == (size_t) -1) {
	hashqueue_push_valid(&frontier, &nodes[i_successors + i]);
      }
    }
  }
}

#include <stdio.h>

int _main(void) {
  // test_equal any
  char state[] = {4, 3, 2, 1, 0};
  char test_eq[][5] = {
    2, 3, 4, 1, 3,
    3, 2, 1, 0, 4,
    4, 3, 2, 1, 0,
  };
  assert(equal_any(state, test_eq, 3));

  char test_neq[][5] = {
    3, 2, 4, 1, 2,
    5, 3, 2, 1, 3,
    1, 1, 1, 1, 1,
    3, 4, 5, 2, 1,
  };
  assert(!equal_any(state, test_neq, 4));

  // HashMap
  size_t hash_i = 0;
  HashMap hashmap = hashmap_create();
  for(size_t i = 0; i < 3; ++i) {
    hashmap_set(&hashmap, test_eq[i], i);
  }
  for(size_t i = 0; i < 3; ++i) {
    assert(i == hashmap_get(&hashmap, test_eq[i], &hash_i));
  }
  assert((size_t) -1 == hashmap_get(&hashmap, test_neq[0], &hash_i));

  // Queue
  Queue queue = queue_create();
  Node nodes[7] = {
    {.state = {2, 3, 4, 1, 3}, .cost = 5},
    {.state = {3, 2, 1, 0, 4}, .cost = 1},
    {.state = {4, 3, 2, 1, 0}, .cost = 0},
    {.state = {3, 2, 4, 1, 2}, .cost = 2},
    {.state = {5, 3, 2, 1, 3}, .cost = 3},
    {.state = {1, 1, 1, 1, 1}, .cost = 2},
    {.state = {3, 4, 5, 2, 1}, .cost = 1},
  };
  assert(queue_pop(&queue) == NULL);

  Node new_nodes[3] = {
    {.state = {2, 3, 4, 1, 3}, .cost = 1},
    {.state = {3, 2, 1, 0, 4}, .cost = 4},
    {.state = {4, 3, 2, 1, 0}, .cost = 0},
  };

  for(size_t i = 0; i < 7; ++i) {
    queue_push(&queue, &nodes[i]);
  }
  Node *curr_node;
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 0);
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 1);
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 1);
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 2);

  queue_push(&queue, &new_nodes[0]);

  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 1);

  queue_push(&queue, &new_nodes[1]);

  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 2);
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 3);

  queue_push(&queue, &new_nodes[2]);
  
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 0);
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 4);
  curr_node = queue_pop(&queue);
  assert(curr_node->cost == 5);
  curr_node = queue_pop(&queue);
  assert(curr_node == NULL);

  printf("Tests passed!\n");
  return 0;
}
