void print_node(Pathnode node) {
  switch (node.state.pos) {
  case STORE:
    printf("Pos: S, ");
    break;
  case A:
    printf("Pos: A, ");
    break;
  case B:
    printf("Pos: B, ");
    break;
  case C:
    printf("Pos: C, ");
    break;
  default:
    fprintf(stderr, "Invalid state for position: %d\n", node.state.pos);
    exit(1);
  }
  printf("A: %d, B: %d, C: %d, Truck: %d, ", node.state.A, node.state.B, node.state.C, node.state.truck);
  printf("Cost: %d, Parent: %p\n", node.cost, node.parent);
}

// test_successors
int main2(void) {
  Pathnode node;
  node.parent = NULL;
  node.cost = 9;
  node.state.A = 2;
  node.state.B = 2;
  node.state.C = 2;
  node.state.truck = 2;
  node.state.pos = (Pos)0;

  print_node(node);
  auto succ = successors(&node);
  for(auto node: succ) {
    print_node(node);
  }
}
